#ifndef __SETTINGS_H__
#define __SETTINGS_H__

#include <EEPROMex.h>
#include <EEPROMVar.h>

#define NPRESETS 30
#define PRESETNAMESIZE 26

#define DUMMYLOADER

struct Preset {
   byte pot50;
   byte pot120;
   byte pot400;
   byte pot500;
   byte pot800;
   byte pot45k;
   byte pot10k;
   byte potLevel;
   char presetName[PRESETNAMESIZE];   
};

struct Settings {
  Preset presets[NPRESETS];
  int lastPreset;
};

class SettingsManager {
public:
  static SettingsManager* getInstance();
  Preset RetrievePreset(int, bool *);
  void ModifyPreset(int, Preset, bool *);
#ifdef DUMMYLOADER
  void loadDummyData();
#endif
	static void printPreset(Preset p);

private:
  SettingsManager();
  ~SettingsManager();

  SettingsManager(const SettingsManager&){}
  
  static SettingsManager *m_instance;
  int address = 0;
};

#endif
