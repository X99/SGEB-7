#include "SettingsManager.h"

SettingsManager* SettingsManager::m_instance = 0;

SettingsManager::SettingsManager() {
	EEPROM.setMemPool(address, EEPROMSizeATmega1280);
}

SettingsManager::~SettingsManager() {
}

SettingsManager* SettingsManager::getInstance() {
	if (m_instance == 0) {
		m_instance = new SettingsManager();
	}
	return m_instance;
}

Preset SettingsManager::RetrievePreset(int number, bool *ok) {
	Preset p = {};

	*ok = (number < NPRESETS ? EEPROM.readBlock(address+(sizeof(Preset)*number), &p, 1) == sizeof(Preset):false);

	return p;
}

void SettingsManager::ModifyPreset(int number, Preset p, bool *ok) {
	*ok = (number < NPRESETS ? EEPROM.writeBlock(address+(sizeof(Preset)*number), &p, 1) == sizeof(Preset):false);
}

void SettingsManager::printPreset(Preset p) {
	Serial.print("byte pot50 = "); Serial.println(p.pot50);
	Serial.print("byte pot120 = "); Serial.println(p.pot120);
	Serial.print("byte pot400 = "); Serial.println(p.pot400);
	Serial.print("byte pot500 = "); Serial.println(p.pot500);
	Serial.print("byte pot800 = "); Serial.println(p.pot800);
	Serial.print("byte pot45k = "); Serial.println(p.pot45k);
	Serial.print("byte pot10k = "); Serial.println(p.pot10k);
	Serial.print("byte potLevel = "); Serial.println(p.potLevel);
	Serial.print("char presetName[] = "); Serial.println(p.presetName);
}


#ifdef DUMMYLOADER
void SettingsManager::loadDummyData() {
	Preset p[] = {
		{10, 20, 30, 40, 50, 60, 70, 80, "CONSECTETUER ADIPISCING E"},
		{10, 20, 30, 40, 50, 60, 70, 80, "JUSTO"},
		{10, 20, 30, 40, 50, 60, 70, 80, "RHONCUS"},
		{10, 20, 30, 40, 50, 60, 70, 80, "TURPIS SED ANTE"},
		{10, 20, 30, 40, 50, 60, 70, 80, "VELIT EU EST"},
		{10, 20, 30, 40, 50, 60, 70, 80, "DICTUMST ALIQUAM AUGUE"},
		{10, 20, 30, 40, 50, 60, 70, 80, "INTEGER NON VELIT"},
		{10, 20, 30, 40, 50, 60, 70, 80, "HAC HABITASSE PLATEA"},
		{10, 20, 30, 40, 50, 60, 70, 80, "IN FELIS"},
		{10, 20, 30, 40, 50, 60, 70, 80, "LIGULA"},
		{10, 20, 30, 40, 50, 60, 70, 80, "ERAT FERMENTUM"},
		{10, 20, 30, 40, 50, 60, 70, 80, "MAGNIS DIS PARTURIENT"},
		{10, 20, 30, 40, 50, 60, 70, 80, "AUGUE"},
		{10, 20, 30, 40, 50, 60, 70, 80, "ODIO"},
		{10, 20, 30, 40, 50, 60, 70, 80, "LIGULA VEHICULA CONSEQUAT"},
		{10, 20, 30, 40, 50, 60, 70, 80, "NULLA"},
		{10, 20, 30, 40, 50, 60, 70, 80, "QUAM NEC"},
		{10, 20, 30, 40, 50, 60, 70, 80, "LIGULA PELLENTESQUE"},
		{10, 20, 30, 40, 50, 60, 70, 80, "FUSCE LACUS"},
		{10, 20, 30, 40, 50, 60, 70, 80, "QUISQUE ARCU LIBERO"},
		{10, 20, 30, 40, 50, 60, 70, 80, "SIT AMET ERAT"},
		{10, 20, 30, 40, 50, 60, 70, 80, "IPSUM PRIMIS"},
		{10, 20, 30, 40, 50, 60, 70, 80, "EGET"},
		{10, 20, 30, 40, 50, 60, 70, 80, "UT ODIO"},
		{10, 20, 30, 40, 50, 60, 70, 80, "MASSA"},
		{10, 20, 30, 40, 50, 60, 70, 80, "TINCIDUNT ANTE"},
		{10, 20, 30, 40, 50, 60, 70, 80, "VESTIBULUM VESTIBULUM"},
		{10, 20, 30, 40, 50, 60, 70, 80, "HAC HABITASSE"},
		{10, 20, 30, 40, 50, 60, 70, 80, "ET"},
		{10, 20, 30, 40, 50, 60, 70, 80, "AMET NULLA QUISQUE"}
	};
	bool ok = true;
	for (int i=0; i<sizeof(p)/sizeof(p[0]); i++) {
		Serial.println(i);
		ModifyPreset(i, p[i], &ok);
	}
}
#endif
