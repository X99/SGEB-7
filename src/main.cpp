#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#include <SettingsManager.h>
#include <icons.h>

#define DRAW_BOOT_SCREEN	0
#define DRAW_BOOT_PROGRESS	1
#define DRAW_MAIN_MENU  	2
#define DRAW_PRESETS		3

U8G2_ST7920_128X64_F_HW_SPI u8g2(U8G2_R0, 30, U8X8_PIN_NONE);

int whatToDraw = 0;

/********************************************************
 * MCP41010 LIST										*
 ********************************************************/
const int potsCSPins[] = {41, 40, 43, 42, 45, 44, 46, 47};
byte potsValues[] = {0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80};


/********************************************************
 * DRAWING											    *
 ********************************************************/
const byte from = 4;
const byte spacing = 13;
const byte beginAt = 3;
const byte endAt = 54;
const byte frameSpacing = 1;
const byte eqTextYPosition = 58;

uint64_t lastTime = 0;

const char * freqs[] = {"50", "120", "400", "500", "800", "4.5k", "10k", "LVL"};
const int8_t offsets[] = {-2, -4, -4,	-4,	-4,	-4,	 -3, -4};

byte selection = 0;

/********************************************************
 * ENCODER DATA									        *
 ********************************************************/
const byte encoderINT1 = 21;
const byte encoderINT2 = 20;
const byte PIN_LED_R = 9;
const byte PIN_LED_G = 10;
const byte PIN_LED_B = 11;
const byte encoderPush = 3;	// the pin that the pushbutton is attached to

volatile int lastEncoded = 0;
volatile float encoderQuantum = 0.0;
volatile int encoderValue = 0;

const int DEBOUNCE_DELAY = 200;

//these variables must be set to proper values before using the encoder
int encoderMax = 20;
int encoderMin = 0;
bool encoderLoop = true;

Preset presets[NPRESETS];


/********************************************************
 * ENCODER CALLBACKS									*
 ********************************************************/
void nullCallback(int value) {}

void (*encoderRotateCallback)(int) = nullCallback;
void (*encoderPushCallback)(int) = nullCallback;

void updateEncoder() {
	int MSB = digitalRead(encoderINT1); //MSB = most significant bit
	int LSB = digitalRead(encoderINT2); //LSB = least significant bit

	int encoded = (MSB << 1) | LSB; //converting the 2 pin value to single number
	int sum  = (lastEncoded << 2) | encoded; //adding it to the previous encoded value

	if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) encoderQuantum -= 0.50;
	if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) encoderQuantum += 0.50;

	if (encoderQuantum == 1 || encoderQuantum == -1) {
		encoderValue += encoderQuantum;
		encoderQuantum = 0;

		if (encoderValue < encoderMin) encoderValue = (encoderLoop? encoderMax:encoderMin);
		if (encoderValue > encoderMax) encoderValue = (encoderLoop? encoderMin:encoderMax);

		encoderRotateCallback(encoderValue);
	}
	lastEncoded = encoded; //store this value for next time   
}

//Eliminate bounce before we call callback
void pushedEncoder() {
	static uint64_t last_interrupt_time = 0;
	uint64_t interrupt_time = millis();
	// If interrupts come faster than 200ms, assume it's a bounce and ignore
	if (interrupt_time - last_interrupt_time > DEBOUNCE_DELAY) {
		last_interrupt_time = interrupt_time;
		encoderPushCallback(encoderValue);
	}
}

void loadPresets() {
	SettingsManager* sm = SettingsManager::getInstance();
	bool ok=true;

	for (int i=0; i<NPRESETS; i++) {
		Serial.println("Retrieving preset number "+String(i));
		presets[i] = sm->RetrievePreset(i, &ok);
		//sm->printPreset(presets[i]);
	}
}

void presetsListCallback(int value) { 
}

void presetsListPushCallback(int value) {  
}

void presetsList() {
	static int firstPreset = 0;
	static bool firstTime = true;
	//int maxFirstPreset = NPRESETS-8;

	//bool ok=false;
	u8g2.setFont(u8g2_font_tom_thumb_4x6_tr);

	//SettingsManager* sm = SettingsManager::getInstance();

	for (int i=firstPreset; i<(firstPreset+4); i++) {
		String str = String(i)+":"+presets[i].presetName;
		u8g2.drawStr(10, 1+(8*i), str.c_str());
	}

	if (firstTime == true) {
		encoderMin = 0;
		encoderMax = NPRESETS-1;
		encoderLoop = false;
		encoderValue = 0;

		//activate encoder
		encoderRotateCallback = presetsListCallback;
		encoderPushCallback = presetsListPushCallback;

		firstTime = false;
	}
}

void mainMenuPushCallback(int value) {
	if (value == 10) {
		whatToDraw = DRAW_PRESETS;
	}
}

void mainMenuCallback(int value) {
	Serial.println(value);
}

void drawEq() {
	byte moreSpace = 0;

	u8g2.setFont(u8g2_font_u8glib_4_tf);

	for (int n=0; n<8; n++) {
		if (n == 7)
			moreSpace = 0.5;
		int x = from + ((n+moreSpace)*spacing);

		//draw slider
		u8g2.drawLine(x, beginAt+1, x, endAt-1);
		u8g2.drawLine(x+1, beginAt, x+1, endAt);
		u8g2.drawLine(x+2, beginAt+1, x+2, endAt-1);

		//draw tick
		int scaledValue = endAt-(((endAt-beginAt)*potsValues[n])/255);
		u8g2.setDrawColor(2);
		u8g2.drawLine(x, scaledValue, x+2, scaledValue);

		//draw selection
		if ((n == encoderValue) && (encoderValue < 8)) {
			u8g2.drawFrame(x-2, beginAt-2, 7, 5+endAt-beginAt);
		}

		//draw eq texts
		u8g2.drawStr(x+offsets[n], eqTextYPosition, freqs[n]);
	}
}

/**
 * @brief Draws main menu (equalizer + icons) and handles selection
 */
void mainMenu() {
	static bool firstTime = true;

	drawEq();

	// draw side icons
	u8g2.drawXBMP(106, 12, 16, 16, load);
	u8g2.drawXBMP(106, 32, 16, 16, save);

	// if side icons are selected
	if (encoderValue > 7 && encoderValue < 10) {
		int y = 10+(encoderValue-8)*20;
		u8g2.drawFrame(104, y, 19, 19);
	}

	//set encoder parameters
	if (firstTime == true) {
		// setup encoder routine
		encoderMin = 0;
		encoderMax = 9;
		encoderLoop = true;
		encoderValue = 0;

		//activate encoder
		encoderRotateCallback = mainMenuCallback;
		encoderPushCallback = mainMenuPushCallback;

		firstTime = false;
	}
}

void showLogo() {
	u8g2.setFontRefHeightExtendedText();
	u8g2.setDrawColor(1);
	u8g2.setFontPosTop();
	u8g2.setFontDirection(0);

	u8g2.setFont(u8g2_font_artossans8_8r);
	u8g2.drawStr(4, 50, "imagine");
	u8g2.drawStr(90, 50, "more");

	int x = 36;
	int y = 4;
	u8g2.drawLine(x+1, y+26, x+6, y+19);
	u8g2.drawLine(x+6, y+19, x+20, y+21);
	u8g2.drawLine(x+20, y+21, x+31, y+7);
	u8g2.drawLine(x+31, y+7, x+57, y+2);
	u8g2.drawLine(x+57, y+2, x+37, y+29);
	u8g2.drawLine(x+37, y+29, x+45, y+57);
	u8g2.drawLine(x+45, y+57, x+31, y+49);
	u8g2.drawLine(x+31, y+49, x+31, y+43);
	u8g2.drawLine(x+31, y+43, x+14, y+38);
	u8g2.drawLine(x+14, y+38, x+6, y+19);
	u8g2.drawLine(x+1, y+26, x+8, y+25);
	u8g2.drawLine(x+53, y+8, x+60, y+7);
	u8g2.drawLine(x+60, y+7, x+45, y+19);
	u8g2.drawLine(x+31, y+7, x+37, y+29);
	u8g2.drawLine(x+37, y+29, x+31, y+49);
	u8g2.drawLine(x+20, y+21, x+14, y+38);
	u8g2.drawLine(x+14, y+38, x+37, y+29);
	u8g2.drawLine(x+20, y+21, x+14, y+38);
}

void changeEncoderColor(byte r, byte g, byte b) {
	analogWrite(PIN_LED_R, ~r);
	analogWrite(PIN_LED_G, ~g);
	analogWrite(PIN_LED_B, ~b);
}


void setPot(uint8_t cspin, uint8_t value) {
	SPI.begin();
	digitalWrite(cspin, LOW);
	SPI.transfer(B00010001);
	SPI.transfer(value);  
	digitalWrite(cspin, HIGH);
	SPI.end();
}

void initPhase() {
	//Encoder setup
	digitalWrite(encoderINT1, HIGH); //turn pullup resistor on
	digitalWrite(encoderINT2, HIGH); //turn pullup resistor on

	attachInterrupt(digitalPinToInterrupt(encoderINT1), updateEncoder, CHANGE);
	attachInterrupt(digitalPinToInterrupt(encoderINT2), updateEncoder, CHANGE);  
	attachInterrupt(digitalPinToInterrupt(encoderPush), pushedEncoder, FALLING);  

	//pots setup
	SPI.begin();
	for(int i=0; i<8; i++) {
		pinMode(potsCSPins[i], OUTPUT);
		digitalWrite(potsCSPins[i], HIGH); 
	}

	/*test MCPs*/
	for (int i=0; i<8; i++) {
		//Serial.print("Setting pot " + String(i) + " to 128");
		setPot(potsCSPins[i], 0); 
		//Serial.println(" Done!");   
	}
}

void draw() {
	u8g2.clearBuffer();
	switch(whatToDraw) {
		case DRAW_BOOT_SCREEN:
			showLogo();
			whatToDraw = DRAW_BOOT_PROGRESS;
			break;
		case DRAW_BOOT_PROGRESS:
			initPhase();
			//loadPresets();
			//whatToDraw = DRAW_MAIN_MENU;
			break;
		case DRAW_MAIN_MENU:
			mainMenu();
			break;
		case DRAW_PRESETS:
			presetsList();
			break;
	}
	u8g2.sendBuffer();
}

void setup() {
	Serial.begin(9600);

	u8g2.begin();
}

void loop() {
	draw();
	// delay between each page
	delay(100);
}