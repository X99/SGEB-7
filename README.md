<div align="left">
    <img src='https://img.shields.io/badge/Version-0.0.1-red'/>
    <img src='https://img.shields.io/badge/Arduino%20Mega-%E2%9C%93%20compatible-brightgreen?style=flat'>
</div>

<div align="center">
	<img src="logo.svg" width="20%" alt="project's logo" style='fill: #94d31b'>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=bugs'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=code_smells'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=coverage'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=duplicated_lines_density'/>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=ncloc'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=sqale_rating'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=alert_status'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=reliability_rating'/>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=security_rating'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=sqale_index'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=SGEB-7&metric=vulnerabilities'/>
</div>

# SGEB-7

SGEB-7 project is, basically, a supercharged bass equalizer created from a **GEB-7 stompbox** from Boss:

![Boss GEB-7](http://cdn.roland.com/assets/images/products/main/geb_7_main.jpg)

Here are the steps:

First, I need to open the stombox, and understand how it works. What I want is to make it "programmable": all these sliders can be replaces by numerical potentiometers, and these nifty buddies can be controlled by an arduino.

Next, find the parts: the sliders need to be replaces by equivalent numerical potentiometers, the case itself needs to be replaced, and so on...

Then, imagine how all these gizmos will fit together, create man-machine-interfaces...you get it...

This repo thus contains everything you need to follow my works. This readme file will also be used as a timeline, I will explain here everything I do, my mistakes, my choices, everything.

I will also post pictures.

## General idea

I will use an arduino as the brain of my futur stompbox. I want it to have a nice LCD, pushbuttons to control it, a rotary encoder to set values.

Everything must use a regular 9V power supply.

All this will fit into a rock-solid metal case.

## Tearing apart the stombox.

Boss stompboxes are really easy to open: four screws and tada, you're done.

Here are the inners:

![Pedal PCB](https://gitlab.com/X99/SGED-7/raw/master/images/IMG_1958.JPG)

Nothing really impressive here: the main PCB is all about opamps, the "control" PCB is composed of 8 potentiometers. I removed one of these to check it out:

![Pedal PCB](https://gitlab.com/X99/SGED-7/raw/master/images/IMG_1959.JPG)

Ok, 10k potentiometer, easy to replace. The idea is to use a numeric potentiometer instead of these ones. After some researches, I found this one could fit my needs:

![Pedal PCB](https://gitlab.com/X99/SGED-7/raw/master/images/MCP41010.jpg)

Let's check what the [datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/11195c.pdf) says:

- 10kOhms
- 256 taps ( = possible values)
- uses SPI, so they can easily be chained
- need at least 2.7V, at max 5.5V power supply.

This makes this chip the ideal for this project. The wiring is very simple, I will explain all of this later on.

## Use cases
### Rough idea
I want the LCD to display something like this:

```
 V
 |  |  |  |  |  |  |     |
 |  |  |  |  |  -  |     |
 |  |  |  |  -  |  -     |
 -  |  |  -  |  |  |     -
 |  -  -  |  |  |  |     |
 |  |  |  |  |  |  |     |
 |  |  |  |  |  |  |     |
 10 20 80 1k 4k 8k 10k  LVL
```

In a graphical manner, of course. The "V" is a mark to show which channel is active. I want to have two buttons, LEFT and RIGHT to choose the channel to modify and a rotary encoder to change the value of the active channel.

### Requirements and priorities

The finished product:

- will offer a "manual mode", in which user manually sets everything he needs.
- will offer a "preset mode" in which he can choose from a list of presets and load it into manual mode.

These presets can be edited once the pedal is plugged to a computer using a software.

# Credits

<div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>