#include <EEPROMex.h>
#include <EEPROMVar.h>

#define NPRESETS 90

struct Preset {
   byte pot50;
   byte pot120;
   byte pot400;
   byte pot500;
   byte pot800;
   byte pot45k;
   byte pot10k;
   byte potLevel;
   char presetName[31];   
};

void printPreset(Preset p) {
  Serial.println("-----------------------------------------");
  Serial.println("Preset: "+String(p.presetName));
  Serial.println("pot = "+String(p.pot50));
  Serial.println("pot = "+String(p.pot120));
  Serial.println("pot = "+String(p.pot400));
  Serial.println("pot = "+String(p.pot500));
  Serial.println("pot = "+String(p.pot800));
  Serial.println("pot = "+String(p.pot45k));
  Serial.println("pot = "+String(p.pot10k));
  Serial.println("pot = "+String(p.potLevel));
}

Preset presets[NPRESETS];
Preset presetsOUT[NPRESETS];

int address = 0;

void setup() {
  Serial.begin(9600);

  Serial.println("\n\n\nSetting up EEPROMEx");
  EEPROM.setMemPool(address, EEPROMSizeATmega1280);
  
  Serial.println("Setting up "+String(NPRESETS)+" presets");
  for (int i=0; i<NPRESETS; i++) {
    presets[i].pot50 = 10;
    presets[i].pot120 = 20;
    presets[i].pot400 = 30;
    presets[i].pot500 = 40;
    presets[i].pot800 = 50;
    presets[i].pot45k = 60;
    presets[i].pot10k = 70;
    presets[i].potLevel = 80;
    strcpy(presets[i].presetName, "A preset");
  }
  
  Serial.println("Size of a preset: "+String(sizeof(Preset)));
  
  Serial.println("Writing presets to EEPROM");
  EEPROM.writeBlock(address, presets, NPRESETS);

  Serial.println("Reading presets from EEPROM");
  EEPROM.readBlock(address, presetsOUT, NPRESETS);

  Serial.println("Checking presets:");
  for (int i=0; i<NPRESETS; i++) {
    Serial.print("Preset n°"+String(i)+" :");
    if (!memcmp(&presets[i], &presetsOUT[i], sizeof(Preset))) 
      Serial.println("OK");
    else 
      Serial.println("NOK");
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
