#include <SPI.h>

const int potsCSPins[] = {41, 40, 43, 42, 45, 44, 46, 47};
const int value = 255;

void setup() {
  Serial.begin(9600);
  
  //pots setup
  for(int i=0; i<8; i++) {
    pinMode(potsCSPins[i], OUTPUT);
    digitalWrite(potsCSPins[i], HIGH); 
  }

  SPI.begin();

  /*test MCPs*/
  for (int i=0; i<8; i++) {
    Serial.print("Setting pot " + String(i) + " to " + String(value));
    setPot(potsCSPins[i], value); 
    Serial.println(" Done!");   
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}

void setPot(uint8_t cspin, uint8_t value) {
  digitalWrite(cspin, LOW);
  SPI.transfer(B00010001);
  SPI.transfer(value);  
  digitalWrite(cspin, HIGH);
}
