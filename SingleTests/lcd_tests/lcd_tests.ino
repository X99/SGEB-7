#include <U8glib.h>
#include <SPI.h>

//LCD. Constructor: SCK, MOSI, CS
U8GLIB_ST7920_128X64 u8g(52, 51, 30, U8G_PIN_NONE);

void setup() {
  delay(1000);
  u8g.begin();
}

void loop() {
  u8g.firstPage();  
  do {
    drawLogo();
  } while (u8g.nextPage());
}

void drawLogo() {
  u8g.setFont(u8g_font_gdr25r);
  u8g.drawStr(7, 30, "SGEB-7");
  
  u8g.setFont(u8g_font_profont12);
  u8g.drawStr(35,47,"Stands for");
  u8g.drawStr(25,60,"\"Super GEB-7\"");
}

