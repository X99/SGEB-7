
const byte encoderINT1 = 20;
const byte encoderINT2 = 21;


void setup() {
  Serial.begin(9600);

  digitalWrite(encoderINT1, HIGH); //turn pullup resistor on
  digitalWrite(encoderINT2, HIGH); //turn pullup resistor on
  
  attachInterrupt(digitalPinToInterrupt(encoderINT1), updateEncoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encoderINT2), updateEncoder, CHANGE);  
}

void loop() {
}



void callbackFunction(int value) {
  Serial.println(value);
}

volatile int lastEncoded = 0;
volatile float encoderQuantum = 0.0;

int encoderMax = 20;
int encoderMin = 0;
bool encoderLoop = true;
void (*encoderCallback)(int) = callbackFunction;
volatile int encoderValue = 0;

void updateEncoder() {
  int MSB = digitalRead(encoderINT1); //MSB = most significant bit
  int LSB = digitalRead(encoderINT2); //LSB = least significant bit

  int encoded = (MSB << 1) | LSB; //converting the 2 pin value to single number
  int sum  = (lastEncoded << 2) | encoded; //adding it to the previous encoded value

  if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) encoderQuantum -= 0.25;
  if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) encoderQuantum += 0.25;

  if (encoderQuantum == 1 || encoderQuantum == -1) {
    encoderValue += encoderQuantum;
    encoderQuantum = 0;
    
    if (encoderValue < encoderMin) encoderValue = (encoderLoop? encoderMax:encoderMin);
    if (encoderValue > encoderMax) encoderValue = (encoderLoop? encoderMin:encoderMax);
      
    encoderCallback(encoderValue);
  }
 
  lastEncoded = encoded; //store this value for next time   
}
