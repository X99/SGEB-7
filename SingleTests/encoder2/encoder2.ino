const byte encoder0PinA = 20;
const byte encoder0PinB = 21;
/*
volatile byte encoder0Pos = 0;

void setup() {
  attachInterrupt(digitalPinToInterrupt(20), doEncoderA, CHANGE);
  attachInterrupt(digitalPinToInterrupt(21), doEncoderB, CHANGE);  

  Serial.begin (9600);
  Serial.println("Starting");
}

void loop(){
}

void doEncoderA(){
  // look for a low-to-high on channel A
  if (digitalRead(encoder0PinA) == HIGH) { 
    // check channel B to see which way encoder is turning
    if (digitalRead(encoder0PinB) == LOW) {  
      encoder0Pos++;         // CW
    } else {
      encoder0Pos--;         // CCW
    }
  } else {  // must be a high-to-low edge on channel A                                       
    // check channel B to see which way encoder is turning  
    if (digitalRead(encoder0PinB) == HIGH) {   
      encoder0Pos = encoder0Pos + 1;          // CW
    } 
    else {
      encoder0Pos = encoder0Pos - 1;          // CCW
    }
  }
  Serial.println (encoder0Pos, DEC);          
}

void doEncoderB(){
  if (digitalRead(encoder0PinB) == HIGH) {   
    if (digitalRead(encoder0PinA) == HIGH) {  
      encoder0Pos = encoder0Pos + 1;         // CW
    } else {
      encoder0Pos = encoder0Pos - 1;         // CCW
    }
  } else { 
    if (digitalRead(encoder0PinA) == LOW) {   
      encoder0Pos = encoder0Pos + 1;          // CW
    } else {
      encoder0Pos = encoder0Pos - 1;          // CCW
    }
  }
  Serial.println (encoder0Pos, DEC);          
} 

*/
/*
#include <Encoder.h>

// Change these pin numbers to the pins connected to your encoder.
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability
Encoder knobLeft(5, 6);
Encoder knobRight(7, 8);
//   avoid using pins with LEDs attached

void setup() {
  Serial.begin(9600);
  Serial.println("TwoKnobs Encoder Test:");
}

long positionLeft  = -999;
long positionRight = -999;

void loop() {
  long newLeft, newRight;
  newLeft = knobLeft.read();
  newRight = knobRight.read();
  if (newLeft != positionLeft || newRight != positionRight) {
    Serial.print("Left = ");
    Serial.print(newLeft);
    Serial.print(", Right = ");
    Serial.print(newRight);
    Serial.println();
    positionLeft = newLeft;
    positionRight = newRight;
  }
  // if a character is sent from the serial monitor,
  // reset both back to zero.
  if (Serial.available()) {
    Serial.read();
    Serial.println("Reset both knobs to zero");
    knobLeft.write(0);
    knobRight.write(0);
  }
}
*/


int encoderPin1 = 20;
int encoderPin2 = 21;

volatile int lastEncoded = 0;
const int encoderMax = 255;
const int encoderMin = 0;
volatile long encoderValue = 0;

long lastencoderValue = 0;
float encoderQuantum = 0.0;

int lastMSB = 0;
int lastLSB = 0;

void setup() {
  Serial.begin (9600);

  pinMode(encoderPin1, INPUT); 
  pinMode(encoderPin2, INPUT);
  
  digitalWrite(encoderPin1, HIGH); //turn pullup resistor on
  digitalWrite(encoderPin2, HIGH); //turn pullup resistor on

  //call updateEncoder() when any high/low changed seen
  //on interrupt 0 (pin 2), or interrupt 1 (pin 3) 
  attachInterrupt(digitalPinToInterrupt(20), updateEncoder, CHANGE); 
  attachInterrupt(digitalPinToInterrupt(21), updateEncoder, CHANGE);

}

void loop(){ 
}


void updateEncoder() {
  int MSB = digitalRead(encoderPin1); //MSB = most significant bit
  int LSB = digitalRead(encoderPin2); //LSB = least significant bit

  int encoded = (MSB << 1) | LSB; //converting the 2 pin value to single number
  int sum  = (lastEncoded << 2) | encoded; //adding it to the previous encoded value

  if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) encoderQuantum -= 0.5;
  if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) encoderQuantum += 0.5;

  if (encoderQuantum == 1 || encoderQuantum == -1) {
    encoderValue += encoderQuantum;
    encoderQuantum = 0;
    
    if (encoderValue < encoderMin) encoderValue = encoderMax;
    if (encoderValue > encoderMax) encoderValue = encoderMin;
    
    Serial.println(encoderValue);
  }
 
  lastEncoded = encoded; //store this value for next time   
}
