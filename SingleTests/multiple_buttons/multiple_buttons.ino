/*int sensorPin = A2;

const byte BUTTON1 = 1;
const byte BUTTON2 = 2;
const byte BUTTON3 = 3;


void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println("Waiting for button...");
  int bt = waitForButton();
  switch(bt) {
    case BUTTON1: Serial.println("LEFT"); break;
    case BUTTON2: Serial.println("ENTER"); break;
    case BUTTON3: Serial.println("RIGHT"); break;
  }
}

int waitForButton() {
  int sensorValue = 0;
  while(sensorValue < 50) {
    sensorValue = analogRead(sensorPin);
  }

  if (sensorValue > 240 && sensorValue < 260)        //should be 255
    return BUTTON1;
  else if (sensorValue > 500 && sensorValue < 530)   //should be 512
    return BUTTON2;
  else if (sensorValue > 750 && sensorValue < 780)   //should be 767
    return BUTTON3;
  else return 0;
}*/

const byte BUTTON1 = 1;
const byte BUTTON2 = 2;
const byte BUTTON3 = 3;
const int DEBOUNCE_DELAY = 200;


void setup() {
  attachInterrupt(digitalPinToInterrupt(2), button1, FALLING);
  attachInterrupt(digitalPinToInterrupt(3), button2, FALLING);
  attachInterrupt(digitalPinToInterrupt(18), button3, FALLING);
  Serial.begin(9600);
  Serial.println("Waiting for buttons");
}

void loop() {
}


void button1() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > DEBOUNCE_DELAY) {
    last_interrupt_time = interrupt_time;
    
    //interrupt code
    Serial.println("LEFT");
  }
}

void button2() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > DEBOUNCE_DELAY) {
    last_interrupt_time = interrupt_time;
    
    //interrupt code
    Serial.println("RIGHT");
  }
} 

void button3() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > DEBOUNCE_DELAY) {
    last_interrupt_time = interrupt_time;
    
    //interrupt code
    Serial.println("ENTER");
  }
} 
